Benutzte Frameworks
- Twitter Bootstrap 3 ( responsive CSS Framework )
- jQuery ( Javascript Bibliothek )

Das Dateisystem ist für eine Bower installation von jQuery und Bootstrap angelegt.
Siehe hierzu:

https://bower.io/

Css Änderungen können entweder über die Datei main.less, in einer selbst angelegten less Datei die
in der bootstrap.less eingebunden werden muss, oder in der changeable.css gemacht werden.

Less Compiler müssen die Datei bootstrap.less in die Datei main.css kompilieren.

Viel Spaß beim werkeln! ;)

Grüße
Manuel